package main

import (
	"fmt"

	"github.com/dgrijalva/jwt-go"
)

func main() {
	fmt.Println("Generating JWT")

	secret := "SecretToGenerateJWT"

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"device": "domo1",
	})

	signedToken, err := token.SignedString([]byte(secret))

	if err != nil {
		fmt.Println("Error to generate JWT")
		fmt.Println(err.Error())
	}

	fmt.Println(signedToken)

	fmt.Println("Parsing JWT")
	token_parse, err := jwt.Parse(signedToken, func(t *jwt.Token) (interface{}, error) {
		_, ok := token.Method.(*jwt.SigningMethodHMAC)
		if !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(secret + "s"), nil
	})

	if err != nil {
		fmt.Println(err.Error())
	}

	claims, ok := token_parse.Claims.(jwt.MapClaims)
	fmt.Println(claims["device"])
	if ok && token_parse.Valid {
		fmt.Println("Token is validated")
		fmt.Println(claims["device"])
		fmt.Fprintf("%T", claims["device"])
	}

	// token_parse, _ := jwt.Parse(signedToken, nil)
	// claims, _ := token_parse.Claims.(jwt.MapClaims)
	// fmt.Println(claims["device"])
}
