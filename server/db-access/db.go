package db

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"

	_ "github.com/go-sql-driver/mysql"
)

type Secret struct {
	Secret string
}

type Sensors struct {
	Values map[string]interface{} `json:"-"`
}

func AddDeviceToken(device string, token string, secret string) (e error) {
	db, err := sql.Open("mysql", "root:password@tcp(127.0.0.1:3307)/DevicesDB")

	if err != nil {
		return err
	}
	defer db.Close()
	stm, err := db.Prepare("INSERT INTO Devicesv1TBL (device,token,secret) VALUES (?,?,?)")

	if err != nil {
		return err
	}

	_, err = stm.Exec(device, token, secret)

	if err != nil {
		return err
	}
	return nil
}

func GetSecret(device string) (secret string, e error) {
	db, err := sql.Open("mysql", "root:password@tcp(127.0.0.1:3307)/DevicesDB")

	if err != nil {
		return "", err
	}
	defer db.Close()

	res, err := db.Query("SELECT secret FROM Devicesv1TBL WHERE device = ?", device)

	if err != nil {
		return "", err
	}
	var s Secret
	for res.Next() {
		err = res.Scan(&s.Secret)
		if err != nil {
			return "", err
		}
	}
	return s.Secret, nil
}

func CreateDB(dbName string) (e error) {
	db, err := sql.Open("mysql", "root:password@tcp(127.0.0.1:3307)/")

	if err != nil {
		return err
	}
	defer db.Close()

	_, err = db.Exec("CREATE DATABASE " + dbName)
	if err != nil {
		return err
	}
	return nil
}

func AddData(dbName string, body []byte) (e error) {
	s := Sensors{}
	if err := json.Unmarshal(body, &s); err != nil {
		panic(err)
	}

	if err := json.Unmarshal(body, &s.Values); err != nil {
		panic(err)
	}

	fmt.Println(s.Values)
	for k, v := range s.Values {
		fmt.Printf("Key: %v[%T], Value: %v[%T]\n", k, k, v, v)
		ck, _ := CheckTable(dbName, k+"TBL")
		td := Typeof(v)
		if ck == true {
			fmt.Println(k+"TBL", "exist")
		} else {
			fmt.Println(k+"TBL", "doesnt exist")
			if td == "float64" {
				err := CreateTableFloat64(dbName, k+"TBL", k)
				if err != nil {
					log.Println(err.Error())
				}
			}
		}
		//Saving data
	}
	return nil
}

func CheckTable(dbName string, tableName string) (v bool, e error) {
	db, err := sql.Open("mysql", "root:password@tcp(127.0.0.1:3307)/")

	if err != nil {
		//log.Println(err.Error())
		return false, err
	}
	defer db.Close()

	_, err = db.Exec("USE " + dbName)
	if err != nil {
		//log.Println(err.Error())
		return false, err
	}

	// res, err := db.Query("SHOW TABLES LIKE " + "'" + tableName + "'")
	_, err = db.Query("SELECT COLUMNS FROM " + tableName)

	if err != nil {
		//log.Println("Error: ", err.Error())
		return false, nil
	}

	//fmt.Println(res)
	return true, nil

}

func CreateTableFloat64(dbName string, tableName string, sensor string) (e error) {
	db, err := sql.Open("mysql", "root:password@tcp(127.0.0.1:3307)/")
	if err != nil {
		//log.Println(err.Error())
		return err
	}
	defer db.Close()

	_, err = db.Exec("USE " + dbName)
	if err != nil {
		//log.Println(err.Error())
		return err
	}

	_, err = db.Exec("CREATE TABLE " + tableName + " (id INT NOT NULL AUTO_INCREMENT, " + sensor + " DECIMAL(20,10) NOT NULL, servertime TIMESTAMP, PRIMARY KEY(id))")

	if err != nil {
		return err
	}
	return nil
}

func Typeof(v interface{}) string {
	switch v.(type) {
	case int:
		return "int"
	case float64:
		return "float64"
	case string:
		return "string"
	case bool:
		return "bool"
	default:
		return "unknown"
	}
}
