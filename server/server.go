package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	tokenJWT "iotapi.com/server/token"
)

func main() {
	fmt.Println("Server Main")
	router := mux.NewRouter()
	router.HandleFunc("/iotapi/v1/home", Home).Methods("GET")
	router.HandleFunc("/iotapi/v1/token/{device}", tokenJWT.GenerateToken).Methods("GET")
	router.HandleFunc("/iotapi/v1/new", tokenJWT.ValidateToken).Methods("POST")

	err := http.ListenAndServe(":5000", router)
	if err != nil {
		log.Fatal(err.Error())
	}
}

func Home(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("IoT Api Home"))
}
