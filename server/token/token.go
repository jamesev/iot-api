package tokenJWT

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"
	"iotapi.com/server/db-access"
)

type Token struct {
	Token string `json:"token"`
}

func GenerateToken(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	device := vars["device"]

	//Cripto to device
	secret := device + "Cripto"

	//Generating token
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"device": device,
	})

	signed_token, err := token.SignedString([]byte(secret))

	if err != nil {
		log.Println(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Error"))
		return
	}

	tk := Token{
		signed_token,
	}
	signed_token_json, err := json.Marshal(tk)

	if err != nil {
		log.Println(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Error"))
		return
	}

	err = db.CreateDB(device + "DB")

	if err != nil {
		log.Println(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Error"))
		return
	}

	err = db.AddDeviceToken(device, signed_token, secret)
	if err != nil {
		log.Println(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Error"))
		return
	}

	w.WriteHeader(http.StatusAccepted)
	w.Write(signed_token_json)
	return
}

func ValidateToken(w http.ResponseWriter, r *http.Request) {
	header := r.Header.Get("Authorization")
	body, err := ioutil.ReadAll(r.Body)

	if err != nil {
		fmt.Println(err.Error())
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Not Payload"))
		return
	}

	if strings.Contains(header, "Bearer ") {
		signedToken := strings.Replace(header, "Bearer ", "", 1)
		//fmt.Println(tk)
		token_parse, _ := jwt.Parse(signedToken, nil)
		claims, _ := token_parse.Claims.(jwt.MapClaims)
		device := claims["device"].(string)
		fmt.Println(device)
		secret, err := db.GetSecret(device)
		if err != nil {
			fmt.Println(err.Error())
			w.WriteHeader(http.StatusNoContent)
			w.Write([]byte("Error"))
			return
		}
		fmt.Println(secret)
		token_parse, err = jwt.Parse(signedToken, func(t *jwt.Token) (interface{}, error) {
			// _, ok := signedToken.Method.(*jwt.SigningMethodHMAC)
			// if !ok {
			// 	return nil, fmt.Errorf("Unexpected signing method: %v", signedToken.Header["alg"])
			// }
			return []byte(secret), nil
		})

		if err != nil {
			w.WriteHeader(http.StatusNoContent)
			w.Write([]byte("Error"))
			return
		}
		_, ok := token_parse.Claims.(jwt.MapClaims)

		if ok && token_parse.Valid {
			fmt.Println("Token Valid!!")
			//Call function to Proccessing Data and Save Data
			err = db.AddData(device+"DB", body)

			if err != nil {
				w.WriteHeader(http.StatusBadRequest)
				w.Write([]byte("Error"))
				return
			}

			w.WriteHeader(http.StatusAccepted)
			w.Write([]byte("Ok"))
			return
		}
		w.WriteHeader(http.StatusNoContent)
		w.Write([]byte("Error"))
		return

	} else {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Error"))
		return
	}

}
